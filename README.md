# README #

### User Authentication Code A Secured Login System ###
 One of the skill-set that is a must have for any web developer is the ability to code a secure registration and login system. This course is designed to equip you with the fundamental skills needed to create a registration and login system using PHP.
Additional resources, quizzes, and assignments will be given throughout the course to continually test your knowledge.
After completing this course, you would have gained some useful and practical skills that will help you in your goal to become a web developer or give you an edge in your current job.

WHAT'S IN THE COURSE?

    Over 20 lectures and more than 3 hours of content
    Create your first login and registration system
    Create Database table, insert and retrieve record
    Write clean and reusable codes
    Workflow
    Code Refactoring

  COURSE REQUIREMENTS:

     Text Editor
     Understanding of Basic PHP and HTML
     Already Setup PHP Development Environment running PHP 5.5 or higher, with little understanding of PHP (e.g. you should know what is a variable, an array just basic knowledge)

